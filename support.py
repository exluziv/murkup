import sys
from PyQt4 import QtGui, QtCore, uic,Qt
import numpy as np
import cv2
import os
import shutil
import time

SKIP_FRAME_VIDEO = 3







def overwrite(line,value,file_config):
    f = open(file_config,"r")
    text = f.read().split("\n")
    f.close()
    f1 = open(file_config,"w")
    if line == 3:
        f1.write(str(text[0])+"\n")
        f1.write(str(text[1])+"\n")
        f1.write(str(value)+"\n")
        f1.write(str(text[3]))
    if line == 4:
        f1.write(str(text[0])+"\n")
        f1.write(str(text[1])+"\n")
        f1.write(str(text[2])+"\n")
        f1.write(str(value))


def read_config(file_config):
    f = open(file_config,"r")
    text = f.read().split("\n")
    f.close()
    return text


def write_out_massiv(file,out_massiv):
    for i in out_massiv:
        file.write(str(i)+" ")



def output_write(img,out_x,out_y,out_v,f_bool,global_output_path):


    out_massiv = np.zeros(51, dtype = np.int32)
    for i in range(0,17):
        out_massiv[i*3] = out_x[i]
        out_massiv[i*3+1] = out_y[i]
        out_massiv[i*3+2] = out_v[i]

    path = global_output_path +"/joint"+".txt"
    if global_output_path != 0:
        if f_bool:
            try:
                os.mkdir(global_output_path+"/"+"video_frame")
            except Exception as e:
                print("error mkdir")
            src = "video_frame/" + str(img)+".jpg" 
            dst = global_output_path+"/"+"video_frame/"+str(img)+".jpg"
            shutil.copyfile(src,dst)
            img = str(img) + ".jpg"
        print("OK "+ path)
        flag = 0
        new_flag = True
        
        try:
            f = open(path,"r")
            data = f.read().split("\n") 
            f.close()
            i = 0
            max_iter = len(data)
        except Exception as e:
            print("file dont read")
            data = []
        
        f1 = open(path,"w")
        
        for line in data:
            print(str(flag) + "  " + str(line))
            if line == "":
                break
            if line == img:
                f1.write(img+"\n")
                new_flag = False
                flag = flag + 1
                continue
            else:
                if flag == 0:
                    f1.write(line+"\n")
                    continue

            
            if flag == 1:
                write_out_massiv(f1,out_massiv)
                f1.write("\n")
                flag = 0
                continue
            
        if new_flag:
            f1.write(str(img)+"\n")
            write_out_massiv(f1,out_massiv)
        f1.close()
        return True


def video_slise(video_path):
    cap = cv2.VideoCapture(video_path)


    count = 0
    while True:
        ret, frame = cap.read()
        print(count)
        try:
            frame1 = cv2.resize(frame,(5,5),interpolation = cv2.INTER_CUBIC)
        except Exception as e:
            break
        
        if count % SKIP_FRAME_VIDEO == 0:
            str1 = "video_frame/"+str(count) + ".jpg"
            #frame = cv2.resize(frame,(480,360),interpolation = cv2.INTER_CUBIC)
            cv2.imwrite(str1,frame)    
        count += 1

    cap.release()