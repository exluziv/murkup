#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
from PyQt4 import QtGui, QtCore, uic, Qt
from support import overwrite, read_config, output_write, video_slise, write_out_massiv
import numpy as np
import cv2
import os
import shutil
import time


SECTION = 500
SKIP_FRAME_VIDEO = 3


size_window_x = 1500
size_window_y = 700

out_x = np.zeros(17, dtype=np.int32)
out_y = np.zeros(17, dtype=np.int32)
out_v = np.zeros(17, dtype=np.int32)

num_joints = 17
curent_joints = 0
x_13 = 0

img_wirse_x = 0
img_wirse_y = 0
global_scale = 1

global_text_enter_e = None
global_text_enter_e5 = None

global_folder_path = None
global_list_img_name = None
global_curent_num_img = 0

global_joints_mod = True

file_config = "config.txt"

global_output_path = None

global_flag = False
global_flag_write = True

global_next_par = True

line_width = 9

global_color_mod = 1

global_ctrl_flag = False

marker_dict = {0: [1236, 128, 1], 1: [1188, 194, 2], 2: [1162, 271, 3], 3: [1124, 329, 4], 4: [1287, 193, 5], 5: [1315, 269, 6],
               6: [1356, 332, 7], 7: [1202, 359, 8], 8: [1199, 464, 9], 9: [1194, 564, 10], 10: [1273, 370, 11], 11: [1284, 484, 12], 
               12: [1287, 558, 13], 13: [1223, 107, 14], 14: [1245, 107, 15], 15: [1210, 107, 16], 16: [1260, 107, 17]}


def creat_item_of_scene(self_sc):

    list_item = []
    for i in range(0, 17):
        list_item.append(Marker(None, self_sc, marker_dict[i][
                         0], marker_dict[i][1], marker_dict[i][2]))

    return list_item


class MainWindow(QtGui.QMainWindow):

    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        global img_wirse_x
        global img_wirse_y
        global global_scale

        # создание сцены для отображения элементов-рисунков:
        self.scene = Scene(obj=self)

        # создание виджета представления для отображения сцены:
        view = QtGui.QGraphicsView(self.scene, self)
        view.centerOn(0, 0)
        # параметры качества прорисовки для виджета представления:
        #view.setRenderHints(QtGui.QPainter.Antialiasing | QtGui.QPainter.SmoothPixmapTransform)
        # view.setGeometry(0,0,1000,700)
        view.resize(size_window_x, size_window_y)
        # view.setBackgroundBrush(QtGui.QColor(0, 128, 64)) # цвет фона представления
        # self.setCentralWidget(view) # размещение виджета представления в
        # главном окне

        item_img = QtGui.QPixmap("host.jpg")
        item_img = item_img.scaled(size_window_x - 30, size_window_y - 5)
        item = Element_bg(item_img, None, self.scene)
        item.setOffset(-10, -10)

        #pg_img_scene = self.bg_img.scaled(500, 350)
        # Do something to STRART

        self.global_item = Element_bg(QtGui.QPixmap(
            "start.png").scaled(350, 150), None, self.scene)
        self.global_item.setOffset(300, 260)

        self.global_item_kontr = Element_bg(QtGui.QPixmap(
            "kontr.jpg").scaled(450, 600), None, self.scene)
        self.global_item_kontr.setOffset(1010, 40)

        item99 = Checkbox_w("delete joints", self, 1050, 10)
        item99.stateChanged.connect(lambda: self.btnstate(item99))

        self.item20 = Line_edit(self, 1190, 10)

        self.item52 = Line_edit(self, 900, 650, 1)
        self.item53 = Line_edit(self, 850, 30, 2)

        item30 = Button_w("Reset joints", self, 1310, 10)
        QtCore.QObject.connect(item30, QtCore.SIGNAL("clicked()"), self.reset)
        self.visio = False
        item2 = Marker(None, self.scene)

        self.list_item = creat_item_of_scene(self.scene)

    def keyPressEvent_2(self, k):
        global global_curent_num_img
        global line_width
        global global_color_mod
        global global_ctrl_flag

        print(k)

        key_dict = {49: 1, 50: 2, 51: 3, 52: 4, 53: 5, 54: 6}

        if k == 45:
            line_width = line_width - 1
        elif k == 43:
            line_width = line_width + 1

        elif k > 48 and k < 55:
            global_color_mod = key_dict[k]

        elif k == 16777236:   # right
            self.visio = False
            next(self)
        elif k == 16777220:  # enter
            self.visio = True
            next(self)
        elif k == 16777234:  # left
            if global_curent_num_img > 1:
                global_curent_num_img = global_curent_num_img - 2
                overwrite(3, global_curent_num_img, file_config)
                self.visio = False
                next(self)

    def reset(self):
        global curent_joints
        global out_x
        global out_y
        global out_v

        curent_joints = 0

        out_x = np.zeros(17, dtype=np.int32)
        out_y = np.zeros(17, dtype=np.int32)
        out_v = np.zeros(17, dtype=np.int32)

        for i in range(0, 17):
            self.scene.removeItem(self.list_item[i])

        for item in self.scene.list_item_scene:
            self.scene.removeItem(item)

        self.list_item = creat_item_of_scene(self.scene)

    def next_button(self):
        self.visio = True
        next(self)

    def next(self):
        global global_folder_path
        global global_curent_num_img
        global img_wirse_x
        global img_wirse_y
        global global_scale
        global curent_joints

        #/home/maxim.popov/Downloads/images2
        #/home/maxim.popov/Downloads/penn_action/Penn_Action/frames/0001
        global out_x
        global out_y
        global out_v
        global global_joints_mod
        global global_list_img_name
        global global_flag
        global global_flag_write
        global global_next_par

        try:
            text_config = read_config(file_config)
            if global_text_enter_e == text_config[1]:
                global_folder_path = str(text_config[1])
                global_curent_num_img = int(text_config[2])
                global_list_img_name = os.listdir(global_folder_path)
        except Exception as e:
            print("except" + " 408")

        self.item53.setText("No save")
        print(global_folder_path)
        print(global_curent_num_img)

        text_config = read_config(file_config)

        if text_config[0] == "folder":

            if self.visio and global_flag:

                if global_curent_num_img > 0 and global_flag_write:
                    print("gogogogogogogogo")
                    a = output_write(global_list_img_name[
                                     global_curent_num_img - 1], out_x, out_y, out_v, False, global_output_path)
                    if a:
                        # print("f")
                        self.item53.setText("Save")

        else:
            if self.visio and global_flag:
                if global_curent_num_img > 0 and global_flag_write:
                    a = output_write((global_curent_num_img - 1) * SKIP_FRAME_VIDEO,
                                     out_x, out_y, out_v, True, global_output_path)
                    if a:
                        # print("f")
                        self.item53.setText("Save")

                    print("ok")

        global_flag = True

        curent_scale = global_scale
        print(curent_scale)
        curent_wirse_x = img_wirse_x
        curent_wirse_y = img_wirse_y
        curent_joints = 0
        if global_joints_mod:
            out_x = np.zeros(17, dtype=np.int32)
            out_y = np.zeros(17, dtype=np.int32)
            out_v = np.zeros(17, dtype=np.int32)

        self.scene.removeItem(self.global_item)

        for i in range(0, 17):
            self.scene.removeItem(self.list_item[i])

        for item in self.scene.list_item_scene:
            self.scene.removeItem(item)

        if text_config[0] == "folder":

            print("458 " + global_folder_path)
            try:
                print("458 " + global_list_img_name[global_curent_num_img])
                global_flag_write = True
            except Exception as e:
                global_flag_write = False
            str1 = global_folder_path + "/" + \
                global_list_img_name[global_curent_num_img]
            name = global_list_img_name[global_curent_num_img]

        else:
            global_list_img_name = os.listdir("video_frame")
            if global_curent_num_img == len(global_list_img_name) - 1:
                global_flag_write = False
            else:
                global_flag_write = True
            str1 = "video_frame/" + \
                str(global_curent_num_img * SKIP_FRAME_VIDEO) + ".jpg"
            name = str(global_curent_num_img * SKIP_FRAME_VIDEO) + ".jpg"
        bg_img = QtGui.QPixmap(str1)

        # print(str1)

        if bg_img.width() > size_window_x - SECTION - 100 or bg_img.height() > size_window_y - 60:
            # print(size_window_x-SECTION-100)
            # print(size_window_y-60)
            scale_x = (size_window_x - SECTION - 100) / float(bg_img.width())
            scale_y = (size_window_y - 60) / float(bg_img.height())
            # print(scale_x)
            # print(scale_y)
            if scale_x > scale_y:
                bg_img = bg_img.scaled(
                    bg_img.width() * scale_y, bg_img.height() * scale_y)
                global_scale = scale_y
            else:
                bg_img = bg_img.scaled(
                    bg_img.width() * scale_x, bg_img.height() * scale_x)
                global_scale = scale_x
        else:
            global_scale = 1

        pad_top = (size_window_x - SECTION - bg_img.width()) / 2
        img_wirse_x = pad_top

        pad_left = (size_window_y - bg_img.height()) / 2
        img_wirse_y = pad_left

        self.global_item = Element_bg(bg_img, None, self.scene)
        self.global_item.setOffset(pad_top, pad_left)
        self.item52.setText(name)

        if global_joints_mod:
            self.list_item = creat_item_of_scene(self.scene)
        else:
            #base_list_item = creat_item_of_scene()
            self.list_item = []

            for i in range(0, 17):
                if out_x[i] != 0 and out_y[i] != 0:
                    self.list_item.append(Marker(None, self.scene, out_x[
                                          i] * curent_scale + curent_wirse_x, out_y[i] * curent_scale + curent_wirse_y, i + 1, out_v[i]))
                else:
                    self.list_item.append(Marker(None, self.scene, marker_dict[i][
                                          0], marker_dict[i][1], marker_dict[i][2]))  # head

        if global_curent_num_img < len(global_list_img_name):
            global_curent_num_img += 1

        self.item20.setText(str(global_curent_num_img) +
                            " in " + str(len(global_list_img_name)))
        overwrite(3, global_curent_num_img, file_config)

    def btnstate(self, b):
        global global_joints_mod

        if b.isChecked() == True:
            global_joints_mod = True
        else:
            global_joints_mod = False

    def mousePressEvent(self, event):
        global curent_joints

        print(curent_joints)
        if event.button() != QtCore.Qt.LeftButton:
            self.scene.removeItem(self.list_item[curent_joints - 1])


class Scene(QtGui.QGraphicsScene):
    #global_item7 = None

    def __init__(self, parent=None, obj=None):
        QtGui.QGraphicsScene.__init__(self, parent)
        self.dndElement = None
        self.dndPixmap = None
        self.obj = obj
        self.item00 = None
        self.list_item_scene = []

    # операция drag and drop входит в область сцены
    def dragEnterEvent(self, event):
        return

    # операция drag and drop покидает область сцены
    def dragLeaveEvent(self, event):
        return

    # в процессе выполнения операции drag and drop
    def dragMoveEvent(self, event):
        pass

    # завершение операции drag and drop

    def keyPressEvent(self, event):
        k = event.key()
        self.obj.keyPressEvent_2(k)

    def mousePressEvent(self, event):
        global curent_joints
        global out_x
        global out_y
        global out_v
        global img_wirse_y
        global img_wirse_x
        global global_scale

        if curent_joints < num_joints:

            if event.button() != QtCore.Qt.LeftButton:

                print("PRESS" + str(event.scenePos().x()))

                self.list_item_scene.append(Marker(
                    None, self, event.scenePos().x(), event.scenePos().y(), curent_joints + 1, 2))

                if event.scenePos().x() < size_window_x - SECTION:
                    out_x[curent_joints] = int(
                        (event.scenePos().x() - img_wirse_x) / global_scale)
                    out_y[curent_joints] = int(
                        (event.scenePos().y() - img_wirse_y) / global_scale)
                    out_v[curent_joints] = 2
                else:
                    out_x[curent_joints] = 0
                    out_y[curent_joints] = 0
                    out_v[curent_joints] = 0

                curent_joints += 1
                # self.removeItem(self.item4)
            else:
                super(Scene, self).mousePressEvent(event)

        else:

            img_5 = cv2.imread("1760.jpg")
            f_out = np.zeros((13, 2))
            #f_out[:,0] = out_x
            #f_out[:,1] = out_y
            #afd = line_draw(img_5,f_out)
            super(Scene, self).mousePressEvent(event)


class Element(QtGui.QGraphicsPixmapItem):

    def __init__(self, pixmap, parent=None, scene=None):
        QtGui.QGraphicsPixmapItem.__init__(self, pixmap, parent, scene)
        self.setTransformationMode(
            QtCore.Qt.SmoothTransformation)  # качество прорисовки
        # вид курсора мыши над элементом
        self.setCursor(QtCore.Qt.OpenHandCursor)
        # self.setAcceptDrops(True)

    def mousePressEvent(self, event):
        if event.button() != QtCore.Qt.LeftButton:  # только левая клавиша мыши
            event.ignore()
            return
        drag = QtGui.QDrag(event.widget())  # объект Drag
        mime = QtCore.QMimeData()
        drag.setMimeData(mime)
        self.scene().dndElement = self  # запоминаем элемент, который переносится
        self.scene().dndPixmap = self.pixmap()
        # рисунок, отображающийся в процессе переноса
        drag.setPixmap(self.pixmap())

        drag.setHotSpot(QtCore.QPoint(80, 110))  # позиция "ухватки"
        # x = int(self.mapToScene(event.scenePos()).x())
        # y = int(self.mapToScene(event.scenePos()).y())
        # drag.setHotSpot(QtCore.QPoint(x, y))

        # временный "затемнённый" рисунок перетаскиваемой картинки
        tempPixmap = QtGui.QPixmap(self.pixmap())
        #painter = QtGui.QPainter()
        # painter.begin(tempPixmap)
        #painter.fillRect(self.pixmap().rect(), QtGui.QColor(127, 127, 127, 127))
        # painter.end()
        self.setPixmap(tempPixmap)

        drag.start()  # запуск (начало) перетаскивания


class Element_bg(QtGui.QGraphicsPixmapItem):

    def __init__(self, pixmap, parent=None, scene=None):
        QtGui.QGraphicsPixmapItem.__init__(self, pixmap, parent, scene)
        self.setTransformationMode(
            QtCore.Qt.SmoothTransformation)  # качество прорисовки


class Checkbox_w(QtGui.QCheckBox):

    def __init__(self, name, parent=None, x=0, y=0):
        QtGui.QCheckBox.__init__(self, name, parent)
        self.move(x, y)
        self.setChecked(True)
        self.resize(140, 30)


class Button_w(QtGui.QPushButton):

    def __init__(self, name, parent=None, x=0, y=0):
        QtGui.QCheckBox.__init__(self, name, parent)
        self.move(x, y)
        self.resize(140, 30)


class Line_edit(QtGui.QLineEdit):

    def __init__(self, parent=None, x=0, y=0, id=0):
        QtGui.QLineEdit.__init__(self, parent)
        self.move(x, y)
        self.resize(100, 30)
        self.setReadOnly(True)
        self.setAlignment(QtCore.Qt.AlignHCenter)
        if id == 1:
            self.setStyleSheet("background-color: rgb(50,50,50); color: rgb(180,180,180); font-size: 16px; border-bottom: 1px solid rgb(150,150,150); border-right: white;"
                               "border-top: white; border-left: white; border-radius: 0px; font-family: 'Times New Roman', Times, serif; font-style: italic;")
        if id == 2:
            self.setStyleSheet("background-color: rgb(50,50,50); color: rgb(180,180,180); font-size: 30px; border: white; border-radius: 0px;"
                               "font-family: 'Impact', 'Times New Roman', serif;")


class Marker(QtGui.QGraphicsItem):

    def __init__(self, parent=None, scene=None, x=3, y=3, id=0, v=0):
        QtGui.QGraphicsItem.__init__(self,  parent,  scene)
        if x != 3:
            self.setAcceptDrops(True)  # разрешаем таскать
            self.setAcceptHoverEvents(True)
            self.visability = v
            tool_dict = {1: "nose", 2: "r-should", 3: "r-elbow", 4: "r-wrist", 5: "l-should", 6: "l-elbow", 7: "l-wrist", 8: "r-hip",
                         9: "r-knee", 10: "r-ankle", 11: "l-hip", 12: "l-knee", 13: "l-ankle", 14: "r-eye", 15: "l-eye", 16: "r-ear", 17: "l-ear"}

            self.setToolTip(tool_dict[id])

            self.x = x
            self.y = y
            self.id = id

            self.color = QtGui.QColor(255, 0,  0)
            #print("create  markere  id = " + str(id))
            global global_color_mod

            no_repetition = id == 5 or id == 6 or id == 7 or id == 11 or id == 12 or id == 13

            if global_color_mod == 1:
                print("color maod 1")
            if global_color_mod == 2:
                if no_repetition:
                    self.color = QtGui.QColor(252, 255,  0)  # желтый
            if global_color_mod == 3:
                if no_repetition:
                    self.color = QtGui.QColor(0, 0,  0)  # белый
            if global_color_mod == 4:
                if no_repetition:
                    self.color = QtGui.QColor(255, 255,  255)  # черный
            if global_color_mod == 5:
                if no_repetition:
                    self.color = QtGui.QColor(0, 255,  0)  # green
            if global_color_mod == 6:
                if no_repetition:
                    self.color = QtGui.QColor(0, 40,  255)  # blue
            if id == 1:
                self.color = QtGui.QColor(200, 0,  192)

            self.setFlag(QtGui.QGraphicsItem.ItemIsMovable)
            self.pen = QtGui.QPen()
            self.pen.setWidth(2)

            if self.id == 14 or self.id == 15:
                self.pen.setWidth(5)

            self.pen.setBrush(self.color)
            self.color_mod_1 = 1
            self.visability = v

            if v == 1:
                if self.id == 14 or self.id == 15:
                    self.pen.setWidth(3)
                else:
                    self.pen.setWidth(1)
            else:
                if self.id == 14 or self.id == 15:
                    self.pen.setWidth(5)
                else:
                    self.pen.setWidth(2)

            self.pos_x = x
            self.pos_y = x

        else:
            self.x = x
            self.color = QtGui.QColor(25, 250,  0)
            self.paiter_param = None
            #self.pen  =  QtGui.QPen()
            # self.pen.setWidth()
            #

        #item4 = Marker(None, self.scene,100,100)

    def paint(self, painter, option=None, widget=None):
        global line_width

        #print("paint   marker id =  " + str(self.id) +"       mod "+ str(self.color_mod_1))
        if self.x == 3:
            painter.setPen(self.color)
            line = QtCore.QLineF(QtCore.QPointF(size_window_x - SECTION,  0),
                                 QtCore.QPointF(size_window_x - SECTION,  size_window_y - 20))
            painter.drawLine(line)
        else:
            if self.id == 14 or self.id == 15:
                # self.pen.setWidth(5)
                self.color = QtGui.QColor(200, 0,  192)
                self.pen.setBrush(self.color)
                painter.setPen(self.pen)
                painter.drawEllipse(self.x - 3, self.y - 3, 6, 6)
            else:
                if self.id == 16 or self.id == 17:
                    self.color = QtGui.QColor(0, 255,  100)
                    self.pen.setBrush(self.color)
                    self.pen.setWidth(6)
                    painter.setPen(self.pen)

                    painter.drawPoint(self.x, self.y)

                else:
                    if self.color_mod_1 == 1:
                        painter.setPen(self.pen)
                    else:
                        self.pen.setWidth(2)
                        painter.setPen(self.pen)

                    line = QtCore.QLineF(QtCore.QPointF(self.x - line_width,  self.y - line_width),
                                         QtCore.QPointF(self.x + line_width,  self.y + line_width))
                    painter.drawLine(line)
                    line = QtCore.QLineF(QtCore.QPointF(self.x - line_width,  self.y + line_width),
                                         QtCore.QPointF(self.x + line_width,  self.y - line_width))
                    painter.drawLine(line)

    def boundingRect(self):
        global line_width
        if self.x == 3:
            return QtCore.QRectF(size_window_x - SECTION, 5,  10, 600)
        else:
            if self.id == 14 or self.id == 15:
                return QtCore.QRectF(self.x - 5, self.y - 5,  12, 12)
            if self.id == 16 or self.id == 17:
                return QtCore.QRectF(self.x - 4, self.y - 4,  9, 9)
            else:
                return QtCore.QRectF(self.x - line_width, self.y - line_width,  line_width * 2, line_width * 2)

    def hoverMoveEvent(self, e):
        self.color = QtGui.QColor(255,  0,  0)
        self.update()

    def focusInEvent(self, e):
        print("fokus")

    def hoverLeaveEvent(self, e):
        self.color = QtGui.QColor(255,  0,  0)
        self.update()

    def mouseDoubleClickEvent(self, e):
        global out_x
        global out_y
        global out_v

        print("fdfd")

        # если визабилити 2, меняем флаг(визабилити) и вид маркера
        if self.visability == 2:

            if self.id == 14 or self.id == 15:
                self.pen.setWidth(3)
            else:
                self.pen.setWidth(1)
            self.visability = 1
            #print("visability   " + str(self.visability))

        else:  # если визабилити 1, меняем флаг(визабилити) и вид маркера

            if self.id == 14 or self.id == 15:
                self.pen.setWidth(5)
            else:
                self.pen.setWidth(2)

            self.visability = 2

        if e.scenePos().x() < size_window_x - SECTION:
            out_v[self.id - 1] = int(self.visability)
        else:
            out_v[self.id - 1] = 0

        self.update()

    def mouseMoveEvent(self,  e):
        global out_x
        global out_y
        global out_v
        global img_wirse_x
        global img_wirse_y
        global global_scale
        # print(self.sceneBoundingRect())
        #print("x = " + str(e.scenePos().x())+ "  y = " + str(e.scenePos().y())+ "id = "+str(self.id))

        if self.visability == 0 and e.scenePos().x() < size_window_x - SECTION:
            self.visability = 2
        #print("mouse    "+ str(e.scenePos().x())+" " + str(e.scenePos().y()))
        # print(self.pos().x())
        if e.scenePos().x() < size_window_x - SECTION:
            out_x[self.id - 1] = int((e.scenePos().x() -
                                      img_wirse_x) / global_scale)
            out_y[self.id - 1] = int((e.scenePos().y() -
                                      img_wirse_y) / global_scale)
            out_v[self.id - 1] = int(self.visability)
        else:
            out_x[self.id - 1] = 0
            out_x[self.id - 1] = 0
            out_v[self.id - 1] = 0

        print(out_v)

        if self.id == 1:
            x_13 = e.scenePos().x()
        if e.scenePos().x() < 8 or e.scenePos().x() > size_window_x - 60:
            return
        if e.scenePos().y() < 8 or e.scenePos().y() > size_window_y - 50:
            return
        # передаем событие дальше, чтоб таскалось
        QtGui.QGraphicsItem.mouseMoveEvent(self,  e)
        #print("global x = " + str(x_13))


def textchanged(text):
    global global_text_enter_e

    global_text_enter_e = text


def enterPress():
    global global_folder_path
    global global_curent_num_img
    global global_list_img_name
    global global_flag
    if global_folder_path == global_text_enter_e:
        print("text == text")
    else:
        global_flag = False
        f = open(file_config, 'w')
        global_folder_path = global_text_enter_e
        str1 = str(global_folder_path[len(global_folder_path) - 5:])
        if str1.find(".") == -1:
            global_list_img_name = os.listdir(global_folder_path)
            f.write("folder" + "\n")
            f.write(str(global_folder_path) + "\n")
            f.write("0" + "\n")
            f.write("0")
            f.close()
            global_curent_num_img = 0
        else:

            try:
                shutil.rmtree("video_frame")
                # print("fd")
            except Exception as e:
                print("delete dir")

            os.mkdir("video_frame")
            time.sleep(1)
            video_slise(str(global_folder_path))

            global_list_img_name = os.listdir("video_frame")

            f.write("video" + "\n")
            f.write(str(global_folder_path) + "\n")
            f.write("0" + "\n")
            f.write("0")
            f.close()

            global_curent_num_img = 0


def textchanged_e5(text):
    global global_text_enter_e5

    global_text_enter_e5 = text


def enterPress_e5():
    global global_output_path

    if global_output_path == global_text_enter_e5:
        print("text == text")
    else:
        global_output_path = global_text_enter_e5
        overwrite(4, global_output_path, file_config)


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    win = MainWindow()

    widget = QtGui.QWidget()

    widget.resize(1500, 800)

    buttonLeft = QtGui.QPushButton("Next")
    buttonLeft.setFont(QtGui.QFont("Arial", 20))

    e4 = QtGui.QLineEdit()
    e4.setFont(QtGui.QFont("Arial", 11))
    e4.setTextMargins(5, 0, 0, 0)
    global global_folder_path
    global global_text_enter_e

    try:
        config_text = read_config(file_config)
        e4.setText(str(config_text[1]))
        global_folder_path = str(config_text[1])
        global_text_enter_e = str(config_text[1])
    except Exception as e:
        print("file not write")

    e4.textChanged.connect(textchanged)
    e4.editingFinished.connect(enterPress)

    e5 = QtGui.QLineEdit()
    e5.setFont(QtGui.QFont("Arial", 11))
    e5.setTextMargins(5, 0, 0, 0)

    e5.textChanged.connect(textchanged_e5)
    e5.editingFinished.connect(enterPress_e5)

    global global_output_path
    global global_text_enter_e5

    try:
        config_text = read_config(file_config)
        e5.setText(str(config_text[3]))
        global_output_path = str(config_text[3])
        global_text_enter_e5 = str(config_text[3])

    except Exception as e:
        print("file not write")

    l1 = QtGui.QLabel("Input path :")
    l2 = QtGui.QLabel(" Output path :")

    layout = QtGui.QGridLayout()
    layout.addWidget(win, 1, 1, 1, 4)

    layout.addWidget(buttonLeft, 2, 1, 1, 4)
    layout.addWidget(l1, 3, 1)
    layout.addWidget(e4, 3, 2)
    layout.addWidget(l2, 3, 3)
    layout.addWidget(e5, 3, 4)

    widget.setLayout(layout)
    widget.setStyleSheet(open("style.qss", "r").read())
    #QtCore.QObject.connect(buttonLeft ,QtCore.SIGNAL("clicked()"),MainWindow(name = "011.jpg") )
    QtCore.QObject.connect(buttonLeft, QtCore.SIGNAL(
        "clicked()"), win.next_button)
    widget.setWindowTitle("MarkUp")

    widget.show()

    sys.exit(app.exec_())
